#
# Be sure to run `pod lib lint MHGRESTful.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = "MHGRESTful"
  s.version          = "0.2.0"
  s.summary          = "A short description of MHGRESTful."

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!
  s.description      = <<-DESC
                       DESC

  s.homepage         = "https://github.com/<GITHUB_USERNAME>/MHGRESTful"
  # s.screenshots     = "www.example.com/screenshots_1", "www.example.com/screenshots_2"
  s.license          = 'MIT'
  s.author           = { "Heguang Miao" => "hikuimiao@gmail.com" }
  s.source           = { :git => "https://github.com/<GITHUB_USERNAME>/MHGRESTful.git", :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.platform     = :ios, '8.0'
  s.requires_arc = true
  s.default_subspecs = 'OAuth', 'Calendar'

  s.resource_bundles = {
    'MHGRESTful' => ['Pod/Assets/*.png']
  }

  s.public_header_files = 'MHGRESTful/**/*.h'
  s.frameworks = 'UIKit'

  s.subspec 'OAuth' do |ss|
    ss.source_files = 'MHGRESTful/Classes/{MHGOAuth}/**/*'
    ss.dependency 'Locksmith', '~> 2.0.2'
  end

  s.subspec 'Calendar' do |ss|
    ss.dependency 'MHGRESTful/OAuth'
    ss.dependency 'SwiftyJSON', '~> 2.3'
    ss.dependency 'PromiseKit/Foundation', '~> 3.0.1'
    ss.source_files = 'MHGRESTful/Classes/{RESTfulEngine}/**/*'
  end

end
