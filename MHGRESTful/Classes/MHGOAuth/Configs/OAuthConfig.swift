//
//  OAuthConfig.swift
//  MHGOAuth-Swift
//
//  Created by Heguang Miao on 19/11/2015.
//  Copyright © 2015 Heguang Miao. All rights reserved.
//

import Foundation

public protocol OAuthConfig {
    
    var clientId:String{get}
    var clientSecret:String?{get}
    var scopes:[String]?{get}
    
    var authorizeURL:NSURL{get}
    var callbackURI:String{get}
    
    /**
     Compose access token request URL from the authorization code response
     
     - parameter response: Authorize response which contains the autorization code, e.g. code=1234567
     
     - returns: Access token request
     */
    func accessTokenRequestWithAuthorizeResponse(response:String) -> NSURLRequest?
    
    /**
     Get all available tokens from the access token response
     
     - parameter response: The response data of the access token request. e.g. {"access_token":"1234567",xxx}
     
     - returns: TokenBundle that contains all available tokens.
     */
    func extractTokenBundleFromAccessTokenResponse(response:NSData) -> TokenBundle?
    
    func refreshTokenRequest(tokenBundle:TokenBundle) -> NSURLRequest?
    
    func handleRefreshTokenResponse(old:TokenBundle, response:NSData) -> TokenBundle?
}