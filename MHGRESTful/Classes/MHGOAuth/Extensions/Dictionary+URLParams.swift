//
//  Dictionary+HTTPParams.swift
//  MHGOAuth_Swift
//
//  Created by Heguang Miao on 22/11/2015.
//  Copyright © 2015 Heguang Miao. All rights reserved.
//

import Foundation

public extension Dictionary {
    
    /**
     Generate URL parameters using a dictionary
     
     e.g.
     ["redirect_uri":"http://www.google.com","access_token":"1234"]
     => redirect_uri=http%3A%2F%2Fwww.google.com&access_token=1234
     
     - parameter withEncoding: Determine if the name and value should be automatically URL-encoded
     
     - returns: URL parameters string
     */
    func toURLParamString(withEncoding:Bool) -> String? {
        var queryItems = [NSURLQueryItem]()
        for (k,v) in self {
            let key = k as! String
            let val = v as! String
            if withEncoding {
                let encodedKey = key.stringByEscapingForURLArgument()
                let encodedVal = val.stringByEscapingForURLArgument()
                let item = NSURLQueryItem(name: encodedKey!, value: encodedVal)
                queryItems.append(item)
            } else {
                let item = NSURLQueryItem(name: key, value: val)
                queryItems.append(item)
            }
            
        }
        let components = NSURLComponents()
        components.queryItems = queryItems
        return components.query
    }
    
    static func fromURLParams(urlParams:String) -> Dictionary<String,String>? {
        let components = NSURLComponents()
        components.query = urlParams
        let queryItems = components.queryItems
        var dict = Dictionary<String,String>()
        if let queryItems = queryItems {
            for anItem in queryItems {
                let k = anItem.name.stringByUnescapingFromURLArgument()!
                let v = anItem.value?.stringByUnescapingFromURLArgument()
                if let v = v {
                    dict[k] = v
                }
            }
        }
        return dict
    }
}