//
//  String+URLArguments.swift
//  MHGOAuth_Swift
//
//  Created by Heguang Miao on 25/11/2015.
//  Copyright © 2015 Heguang Miao. All rights reserved.
//

import Foundation

public extension String {
    func stringByEscapingForURLArgument() -> String? {
        let escaped = CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, self, nil, "!*'();:@&=+$,/?%#[]", CFStringBuiltInEncodings.UTF8.rawValue)
        return escaped as String
    }
    
    func stringByUnescapingFromURLArgument() -> String? {
        let tmp = self
        let result = tmp.stringByReplacingOccurrencesOfString("+", withString: " ").stringByReplacingPercentEscapesUsingEncoding(NSUTF8StringEncoding)
        return result
    }
}