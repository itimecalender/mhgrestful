//
//  CalendarInfo.swift
//  MHGRESTful
//
//  Created by Heguang Miao on 24/01/2016.
//  Copyright © 2016 Heguang Miao. All rights reserved.
//

import Foundation

public struct CalendarInfo : CustomStringConvertible {
    public var calendarId:String
    public var calendarName:String
    public init(calendarId:String, calendarName:String){
        self.calendarId = calendarId;
        self.calendarName = calendarName;
    }
    
    public var description:String {
        get{
            return "id:\(calendarId)\name:\(calendarName)\n"
        }
    }
}