//
//  TokenBundle.swift
//  MHGOAuth_Swift
//
//  Created by Heguang Miao on 19/11/2015.
//  Copyright © 2015 Heguang Miao. All rights reserved.
//

import Foundation
import Locksmith
/**
 *  Token bundle includes all necessary tokens and associated information about tokens
 */
public struct TokenBundle : CustomStringConvertible, ReadableSecureStorable, CreateableSecureStorable, DeleteableSecureStorable, GenericPasswordSecureStorable {
    
    public var createAt = NSDate()
    public var accessToken:String = ""
    public var accessTokenExpireIn:Int = 0
    public var refreshToken:String?
    public var idToken:String?
    public var idTokenExpireIn:Int = 0
    
    // TODO: add scopes field
    
    /**
     Constructor
     
     - parameter service: The service identifier, e.g. Google, Outlook
     - parameter account: The identifier to discriminate users, for example, user id
     
     */
    public init(service:String, account:String) {
        self.service = service
        self.account = account
    }
    
    public init(service:String) {
        self.init(service:service, account:"default")
    }
    
    /// This is for debugging
    public var description:String {
        get{
            return "createAt:\(createAt)\naccessToken:\(accessToken)\n"
        }
    }
    
    /// Check if the access token is expired
    public var accessTokenExpired:Bool {
        get {
            let now = NSDate()
            let timePassed = now.timeIntervalSinceDate(createAt)
            return timePassed - Double(accessTokenExpireIn) > 0
        }
    }
    
    // MARK: Locksmith
    
    public var service : String
    public var account : String
    
    
    public var data: [String: AnyObject] {
        var dict = ["createAt":createAt, "accessToken":accessToken, "accessTokenExpireIn": accessTokenExpireIn, "idTokenExpireIn":idTokenExpireIn]
        if let refreshToken = refreshToken {
            dict["refreshToken"] = refreshToken
        }
        if let idToken = idToken {
            dict["idToken"] = idToken
        }
        return dict
    }
    
    public func createOrUpdateInSecureStore() -> Bool {
        do {
            try self.createInSecureStore()
            return true
        } catch _ {
            do {
                try self.deleteFromSecureStore()
                try self.createInSecureStore()
                return true
            } catch _ {
                return false
            }
        }
    }
    
    public mutating func restoreFromKeyChain() -> Bool {
        let result = self.readFromSecureStore()
        if let data = result?.data {
            if let createAt = data["createAt"] as? NSDate,
                accessToken = data["accessToken"] as? String,
                accessTokenExpireIn = data["accessTokenExpireIn"] as? Int,
                idTokenExpireIn = data["idTokenExpireIn"] as? Int
            {
                    self.createAt = createAt
                    self.accessToken = accessToken
                    self.accessTokenExpireIn = accessTokenExpireIn
                    self.idTokenExpireIn = idTokenExpireIn
                    if let refreshToken = data["refreshToken"] as? String {
                        self.refreshToken = refreshToken
                    }
                    if let idToken = data["idToken"] as? String {
                        self.idToken = idToken
                    }
                    return true
            }
        }
        return false
    }
}