//
//  OAuthBaseEngine.swift
//  MHGOAuth-Swift
//
//  Created by Heguang Miao on 19/11/2015.
//  Copyright © 2015 Heguang Miao. All rights reserved.
//

import UIKit

public let OAuthEngineErrorDomain = "info.herkuang.OAuth"
public let WrongAuthorizationResponseErrorCode = 100
public let UserCancelOAuthErrorCode = 101
public let RefreshTokenErrorCode = 102


public class OAuthEngine {
    
    private var config:OAuthConfig
    private var callback:((error:NSError?, tokenBundle:TokenBundle?)->Void)?
    private var accessTokenRequestTask:NSURLSessionTask? = nil
    private var refreshTokenRequestTask:NSURLSessionTask? = nil
    
    private var session:NSURLSession = {
        let sessionConfiguration = NSURLSessionConfiguration.defaultSessionConfiguration()
        sessionConfiguration.requestCachePolicy = NSURLRequestCachePolicy.ReloadIgnoringCacheData
        
        return NSURLSession(configuration: sessionConfiguration)
    }()
    
    public init(config:OAuthConfig) {
        self.config = config
    }
    
    // The auth code is in the response.
    // If the response is valid, request for the access token.
    public func authorizeResponse(error:NSError?, resp:String?) {
        if let error = error {
            self.callback?(error: error, tokenBundle: nil)
            return
        }
        if let resp = resp {
            if let request = self.config.accessTokenRequestWithAuthorizeResponse(resp) {
                self.accessTokenRequestTask?.cancel() // cancel former request
                self.accessTokenRequestTask = session.dataTaskWithRequest(request, completionHandler: { (data, response, error) in
                    if let error = error {
                        self.callback?(error: error, tokenBundle: nil)
                    } else {
                        self.callback?(error: nil,
                            tokenBundle: self.config.extractTokenBundleFromAccessTokenResponse(data!))
                    }
                })
                self.accessTokenRequestTask?.resume()
                if self.accessTokenRequestTask != nil {return}
            }
        }
        
        // Other errors
        let errorInfo = [NSLocalizedDescriptionKey:"Wrong authorization response"]
        let e = NSError(domain: OAuthEngineErrorDomain, code: WrongAuthorizationResponseErrorCode, userInfo: errorInfo)

        self.callback?(error: e, tokenBundle: nil)

    }
    
    /**
     Entry point
     
     - parameter callback:    Callback when the oauth finishes
     
     - returns: An OAuthWebViewController instance. 
        The caller should be responsible for displaying this view controller either by pushing it
        into the navigation stack or presenting as modal view controller.
     */
    public func startAuth(callback:(error:NSError?, tokenBundle:TokenBundle?)->Void) -> UIViewController {
        self.callback = callback
        let webVC = OAuthWebViewController()
        webVC.callbackURI = self.config.callbackURI
        webVC.URL = self.config.authorizeURL
        webVC.authorizeResponseBlock = { self.authorizeResponse($0, resp: $1)}
        return webVC
    }
    
    public func refreshToken(oldTokenBundle:TokenBundle, callback:(error:NSError?, newTokenBundle:TokenBundle?)->Void) {
        if let _ = oldTokenBundle.refreshToken {
            if let request = self.config.refreshTokenRequest(oldTokenBundle) {
                self.refreshTokenRequestTask?.cancel()
                self.refreshTokenRequestTask = session.dataTaskWithRequest(request, completionHandler: {
                    (data, response, error) in
                    if let error = error {
                        callback(error: error, newTokenBundle: nil)
                    } else {
                        callback(error: nil,
                            newTokenBundle: self.config.handleRefreshTokenResponse(oldTokenBundle, response: data!))
                    }
                })
                self.refreshTokenRequestTask?.resume()
                if self.refreshTokenRequestTask != nil {return}
            }
        }
        let errorInfo = [NSLocalizedDescriptionKey:"Refresh token failed"]
        let e = NSError(domain: OAuthEngineErrorDomain, code: RefreshTokenErrorCode, userInfo: errorInfo)
        
        callback(error: e, newTokenBundle: nil)
    }
    
    /**
     Cancel the OAuth
     */
    public func cancel() {
        self.accessTokenRequestTask?.cancel()
        self.refreshTokenRequestTask?.cancel()
    }
}