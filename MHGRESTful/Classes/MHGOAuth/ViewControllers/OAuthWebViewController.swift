//
//  OAuthWebViewController.swift
//  MHGOAuth_Swift
//
//  Created by Heguang Miao on 25/11/2015.
//  Copyright © 2015 Heguang Miao. All rights reserved.
//

import UIKit

public class OAuthWebViewController: UIViewController,UIWebViewDelegate {

    var webView:UIWebView = UIWebView()
    public var callbackURI = ""
    
    public var URL:NSURL?
    
    public var authorizeResponseBlock:((NSError?, String?)->Void)?
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.whiteColor()
        self.webView.frame = self.view.bounds
        self.view.addSubview(webView)
        self.webView.autoresizingMask = [UIViewAutoresizing.FlexibleWidth, UIViewAutoresizing.FlexibleHeight]
        self.webView.delegate = self
        
        let dismissButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.Plain, target: self, action: "cancelButtonOnTouch")
        self.navigationItem.rightBarButtonItem = dismissButton
        
        if let URL = self.URL {
            self.webView.loadRequest(NSURLRequest(URL: URL))
        }
    }
    
    override public func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        if let URL = URL {
            self.webView.loadRequest(NSURLRequest(URL:URL))
        }
    }

    override public func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    public func webView(webView: UIWebView, shouldStartLoadWithRequest request: NSURLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        
        // Intercept the request.
        // If the URL has prefix that matches our callbackURI, then the request should not be loaded.
        // Because the authorization code is appended as the params of the request, we should extract
        // it for later usage.
        if let requestURL = request.URL {
            if requestURL.absoluteString.hasPrefix(self.callbackURI) {
                if let authorizeResponseBlock = authorizeResponseBlock {
                    print(requestURL.query)
                    authorizeResponseBlock(nil, requestURL.query)
                    self.dismiss()
                }
                return false
            }
        }
        return true
    }
    
    func cancelButtonOnTouch() {
        // if the user cancels the request, we regard it as a failure.
        let error = NSError(domain: OAuthEngineErrorDomain, code: UserCancelOAuthErrorCode, userInfo: [NSLocalizedDescriptionKey:"User cancelled OAuth"])
        self.authorizeResponseBlock?(error,nil)
        self.dismiss()
    }
    
    func dismiss() {
        
        
        if self.isModal {
            self.dismissViewControllerAnimated(true, completion: nil)
        } else {
            self.navigationController?.popViewControllerAnimated(true)
        }
    
    }
    
    var isModal: Bool {
        return self.presentingViewController?.presentedViewController == self
            || (self.navigationController != nil && self.navigationController?.presentingViewController?.presentedViewController == self.navigationController)
            || self.tabBarController?.presentingViewController is UITabBarController
    }

}
