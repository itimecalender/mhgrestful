//
//  CalendarReader.swift
//  Pods
//
//  Created by Heguang Miao on 4/12/2015.
//
//

import Foundation
import SwiftyJSON
import PromiseKit

public let MHGRESTfulErrorDomain = "info.herkuang.MHGRESTful"
public let CalendarNotFoundErrorCode = 100

public enum CalendarReaderError:ErrorType {
    case ListCalendarFailed(NSData)
    case GetEventsError(NSData)
    case MissingAccessToken
}

/**
 The protocol for calendar readers
 
 Usage:
 
    let gCalendarReader = GoogleCalendarReader(tokenBundle:tokenBundle)
    gCalendarReader.readCalendar().finish({ (events) -> Void in
        print(events)
    }).error({ (error) -> Void in
        print(error)
    })
 
 */
public protocol CalendarReadable {
    
    var timeZone:String {get set}
    
    var tokenBundle:TokenBundle? {get set}
    
    func getEvents() -> Promise<[JSON]>
    
    func getEvents(calendar:CalendarInfo) -> Promise<[JSON]>
    
    func getEvents(calendars: [CalendarInfo]) -> Promise<[JSON]>
    
    func listCalendars() -> Promise<[CalendarInfo]>
    
    func stopAllTasks()
}