//
//  GoogleCalendarReader.swift
//  Pods
//
//  Created by Heguang Miao on 4/12/2015.
//
//

import Foundation
import SwiftyJSON
import PromiseKit

public class GoogleCalendarReader:CalendarReadable {
    
    public var timeZone = "UTC"
    
    public var tokenBundle:TokenBundle? {
        didSet {
            if tokenBundle?.accessToken == oldValue?.accessToken {
                return
            }
            if let tokenBundle = tokenBundle {
                self.stopAllTasks()
                self.URLSession = self.createURLSession(tokenBundle)
            }
            
        }
    }
    private var URLSession:NSURLSession!
    

    private func createURLSession(tokenBundle:TokenBundle?) -> NSURLSession {
        let sessionConfiguration = NSURLSessionConfiguration.defaultSessionConfiguration()
        sessionConfiguration.requestCachePolicy = NSURLRequestCachePolicy.ReloadIgnoringCacheData
        
        // We add the authorization header here for all requests
        if let tokenBundle = tokenBundle {
            sessionConfiguration.HTTPAdditionalHeaders = ["Authorization":tokenBundle.accessToken]
        }
        let session = NSURLSession(configuration: sessionConfiguration)
        return session
    }
    
    public init(tokenBundle:TokenBundle?) {
        self.tokenBundle = tokenBundle
        self.URLSession = self.createURLSession(tokenBundle)
    }
    
    public convenience init() {
        self.init(tokenBundle: nil)
    }
    
    
    private func noAccessTokenWarningIfNecessary() {
        if URLSession.configuration.HTTPAdditionalHeaders?["Authorization"] == nil {
            print("CalendarReader: Your calendar reader doesn't have an access token, which can lead to unauthorized errors!")
        }
    }
    
    private func wrapRequestWithAuthHeader(request:NSMutableURLRequest) {
        if let tokenBundle = self.tokenBundle {
            request.setValue("Authorization", forHTTPHeaderField: tokenBundle.accessToken)
        }
    }
    
    public func listCalendars() -> Promise<[CalendarInfo]> {
        noAccessTokenWarningIfNecessary()
        let request = NSMutableURLRequest(URL: NSURL(string: "https://www.googleapis.com/calendar/v3/users/me/calendarList")!)
        
        return URLSession.promise(request).then { data -> Promise<[CalendarInfo]> in
            let json = JSON(data:data)
            var infoObjs = [CalendarInfo]()
            if let items = json["items"].array where items.count > 0{
                items.forEach { calendarInfo in
                    let anInfoObject = CalendarInfo(calendarId: calendarInfo["id"].string!, calendarName: calendarInfo["summary"].string!)
                    infoObjs.append(anInfoObject)
                }
                
                return Promise<[CalendarInfo]>(infoObjs)
            } else {
                throw CalendarReaderError.ListCalendarFailed(data)
            }
        }
    }
    
    public func getEvents(calendar:CalendarInfo) -> Promise<[JSON]> {
        noAccessTokenWarningIfNecessary()
        let request = NSMutableURLRequest(URL: NSURL(string: "https://www.googleapis.com/calendar/v3/calendars/\(calendar.calendarId.stringByEscapingForURLArgument()!)/events?timeZone=\(self.timeZone)")!)
        return URLSession.promise(request).then({data -> Promise<[JSON]> in
            let json = JSON(data:data)
            if let items = json["items"].array {
                return Promise<[JSON]>(items)
            } else {
                throw CalendarReaderError.GetEventsError(data)
            }
        })
    }
    
    public func getEvents(calendars: [CalendarInfo]) -> Promise<[JSON]> {
        var promises = [Promise<[JSON]>]()
        for calendar in calendars {
            promises.append(getEvents(calendar))
        }
        return when(promises).then { results -> Promise<[JSON]> in
            return Promise<[JSON]>(Array(results.flatten()))
        }
    }
    
    public func getEvents() -> Promise<[JSON]> {
        return listCalendars().then({calendarInfoObjs -> Promise<[JSON]> in
            return self.getEvents(calendarInfoObjs)
        })
    }
    
    public func stopAllTasks() {
        URLSession.getTasksWithCompletionHandler { (tasks, _, _) -> Void in
            tasks.forEach({$0.cancel()})
        }
    }
}