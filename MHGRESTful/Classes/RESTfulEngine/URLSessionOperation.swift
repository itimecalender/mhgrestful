//
//  AlamofireOperation.swift
//  Pods
//
//  Created by Heguang Miao on 5/12/2015.
//
//

import Foundation


public class URLSessionOperation:NSOperation{
    
    enum State {
        case Ready, Executing, Finished
        func keyPath() -> String {
            switch self {
            case Ready:
                return "isReady"
            case Executing:
                return "isExecuting"
            case Finished:
                return "isFinished"
            }
        }
    }
    
    // MARK: - Properties
    
    var state = State.Ready {
        willSet {
            willChangeValueForKey(newValue.keyPath())
            willChangeValueForKey(state.keyPath())
        }
        didSet {
            didChangeValueForKey(oldValue.keyPath())
            didChangeValueForKey(state.keyPath())
        }
    }
    
    // MARK: - NSOperation
    
    public override var ready: Bool {
        return super.ready && state == .Ready
    }
    
    public override var executing: Bool {
        return state == .Executing
    }
    
    public override var finished: Bool {
        return state == .Finished
    }
    
    public override var asynchronous: Bool {
        return true
    }
    
    private var sessionTask:NSURLSessionTask!
    
    public init(session:NSURLSession, request:NSURLRequest, completionHandler:((data:NSData?, response:NSURLResponse?, error:NSError?)->Void)?) {
        super.init()
        self.sessionTask = session.dataTaskWithRequest(request, completionHandler: {[weak self] data, response, error in
            if let sSelf = self, completionHandler = completionHandler where !sSelf.cancelled{
                completionHandler(data: data, response: response, error: error)
                
            }
            self?.state = .Finished
        })
        
        
    }
    
    public override func start() {
        if self.cancelled {
            self.state = .Finished
        } else {
            self.sessionTask.resume()
            self.state = .Executing
        }
    }
    
    public override func cancel() {
        super.cancel()
        self.sessionTask.cancel()
    }
    
}