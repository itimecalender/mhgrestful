//
//  TokenBundleTests.swift
//  MHGRESTful
//
//  Created by Heguang Miao on 14/12/2015.
//  Copyright © 2015 Heguang Miao. All rights reserved.
//

import XCTest
@testable import MHGRESTful

class TokenBundleTests: XCTestCase {

    let TestServiceName = "Google"
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
        var tokenBundle = TokenBundle(service: TestServiceName)
        do {
            try tokenBundle.deleteFromSecureStore()
        } catch _ {
            
        }
        
    }

    func testSaveRestoreToKeyChain() {
        var tokenBundle = TokenBundle(service: TestServiceName)
        tokenBundle.accessToken = "this is an access token"
        tokenBundle.refreshToken = "this is a refresh token"
        XCTAssertTrue(tokenBundle.createOrUpdateInSecureStore())
        
        var restoreBundle = TokenBundle(service:TestServiceName)
        let res = restoreBundle.restoreFromKeyChain()
        XCTAssertTrue(res, "should restore")
        XCTAssertEqual("this is an access token", restoreBundle.accessToken)
        XCTAssertEqual("this is a refresh token", restoreBundle.refreshToken)
    }
    
    func testUpdateTokenBundle() {
        var tokenBundle = TokenBundle(service: TestServiceName)
        tokenBundle.accessToken = "this is an access token"
        tokenBundle.refreshToken = "this is a refresh token"
        do {
            try tokenBundle.createInSecureStore()
        } catch _ {
            XCTAssertFalse(true, "should create in key chain")
        }
        tokenBundle.accessToken = "this is a second access token"
        XCTAssertTrue(tokenBundle.createOrUpdateInSecureStore())
        
        var restoreBundle = TokenBundle(service:TestServiceName)
        let res = restoreBundle.restoreFromKeyChain()
        XCTAssertTrue(res, "should restore")
        XCTAssertEqual("this is a second access token", restoreBundle.accessToken)
    }

}
