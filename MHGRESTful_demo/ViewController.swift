//
//  ViewController.swift
//  MHGOAuthDemo
//
//  Created by Heguang Miao on 19/11/2015.
//  Copyright © 2015 Heguang Miao. All rights reserved.
//

import UIKit
import MHGRESTful
import PromiseKit

class ViewController: UIViewController {

    var oauthEngine:OAuthEngine = {
        var googleConfig = GoogleOAuthConfig(clientId: "534370830160-44e4c37vg89lb5unu7ono9vbp9bg34co.apps.googleusercontent.com",
            clientSecret: "Tb8aNVCVoju8Wkl4exKMrFg7")
        googleConfig.scopes = ["https://www.googleapis.com/auth/calendar.readonly"]
        return OAuthEngine(config:googleConfig)
    }()
    
    var calendarReader : GoogleCalendarReader?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func beginGoogleAuth(sender:UIButton) {
        let webVC = self.oauthEngine.startAuth { (error, tokenBundle) -> Void in
            if let tokenBundle = tokenBundle {
                let gCalendarReader = GoogleCalendarReader()
                gCalendarReader.tokenBundle = tokenBundle
                gCalendarReader.getEvents().then({ jsons in
                    print(jsons)
                }).error({ (err) -> Void in
                    print(err)
                })
                self.calendarReader = gCalendarReader
            }
        }
        let webVCNav = UINavigationController(rootViewController: webVC)
        self.presentViewController(webVCNav, animated: true, completion: nil)
    }
}

