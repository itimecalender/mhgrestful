# MHGRESTful

[![CI Status](http://img.shields.io/travis/Heguang Miao/MHGRESTful.svg?style=flat)](https://travis-ci.org/Heguang Miao/MHGRESTful)
[![Version](https://img.shields.io/cocoapods/v/MHGRESTful.svg?style=flat)](http://cocoapods.org/pods/MHGRESTful)
[![License](https://img.shields.io/cocoapods/l/MHGRESTful.svg?style=flat)](http://cocoapods.org/pods/MHGRESTful)
[![Platform](https://img.shields.io/cocoapods/p/MHGRESTful.svg?style=flat)](http://cocoapods.org/pods/MHGRESTful)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

MHGRESTful is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "MHGRESTful"
```

## Author

Heguang Miao, hikuimiao@gmail.com

## License

MHGRESTful is available under the MIT license. See the LICENSE file for more info.
